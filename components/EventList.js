import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View } from 'react-native';
import HTMLView from 'react-native-htmlview';

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
  },
  itemTitle: {
    fontSize: 18,
  },
  itemDate: {
    fontSize: 12,
  },
})

export class EventList extends Component {
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.events}
          renderItem={({item}) =>
            <View style={styles.item}>
              <Text style={styles.itemTitle}>{item.title}</Text>
              <Text style={styles.itemDate}>{item.timeStart} - {item.timeEnd}</Text>
            </View>
          }
        />
      </View>
    );
  }
}

export default EventList;